#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

lutim_ - Munin plugin to monitor multiple Lutim instances

=head1 AUTHOR AND COPYRIGHT

Copyright 2017 Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/lutim

  [lutim]
     env.urls https://lutim.exemple.org/stats.json https://foobar.org/lutim/stats.json

env.urls must contain the full URLs to the stats.json of your Lutim instances

=item - /etc/munin/plugins

  ln -s lutim /etc/munin/plugins/lutim

=item - restart Munin node

  service munin-node restart

=head1 DEPENDENCIES

You will need the Perl distributions Mojolicious (and IO::Socket::SSL if one of your Lutim instances is available using https).

Install it with

  cpan Mojolicious # (don't use the Debian package, it's too old)

For IO::Socket::SSL, you can do

  apt-get install libio-socket-ssl-perl

or

  cpan IO::Socket::SSL # (may require dependencies to compile)

=head1 LICENSE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use Munin::Plugin;
use Mojo::Base -base;
use Mojo::UserAgent;
use Mojo::URL;

my $PLUGIN_NAME = 'lutim';

munin_exit_fail() unless (defined($ENV{urls}));

my @urls = split(' ', $ENV{urls});

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    for my $url (@urls) {
        my $u    = Mojo::URL->new($url);
        my $host = $u->host;
        $host    =~ s/\./_/g;
        print "multigraph lutim_$host\n";
        print "graph_title $host\n";
        print "graph_vlabel Number of images\n";
        print "graph_args --lower-limit 0\n";
        print "graph_category lutim\n";
        print "$host.label $host\n";
        print "$host.draw AREA\n";

        print "multigraph lutim_${host}_average\n";
        print "graph_title $host - average\n";
        print "graph_vlabel Average number of images uploaded per day\n";
        print "graph_args --lower-limit 0\n";
        print "graph_category lutim\n";
        print "${host}_average.label ${host}_average\n";
        print "${host}_average.draw AREA\n";

        for my $i (qw(day week month year unlimited)) {
            for my $j (qw(enabled disabled)) {
                print "multigraph lutim_${host}_${i}_$j\n";
                print "graph_title $host - $i - $j\n";
                print "graph_vlabel Number of $j images: $i\n";
                print "graph_args --lower-limit 0\n";
                print "graph_category lutim\n";
                print "${host}_${i}_$j.label ${host}_$i\n";
                print "${host}_${i}_$j.draw AREA\n";
            }
        }
    }
    munin_exit_done();
}

##### fetch
my $ua  = Mojo::UserAgent->new;
for my $url (@urls) {
    my $u    = Mojo::URL->new($url);
    my $host = $u->host;
    $host    =~ s/\./_/g;

    my $res = $ua->get($url)->result;
    if ($res->is_error) {
        say $res->message;
        munin_exit_fail();
    }
    my $json = $res->json;

    print "multigraph lutim_$host\n";
    print "$host.value ".$json->{total}."\n";
    print "multigraph lutim_${host}_average\n";
    print "${host}_average.value ".$json->{average}."\n";
    for my $i (qw(day week month year unlimited)) {
        for my $j (qw(enabled disabled)) {
            print "multigraph lutim_${host}_${i}_$j\n";
            print "${host}_${i}_$j.value ".$json->{$i}->{$j}."\n";
        }
    }
}

munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
