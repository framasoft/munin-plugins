#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  jitsi_total_failed_conferences - Used to track the total of failed conferences on Jitsi Meet

=head1 AUTHOR AND COPYRIGHT

  Copyright 2020 Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugins

     ln -s jitsi_total_failed_conferences /etc/munin/plugins/jitsi_total_failed_conferences

=item - restart Munin node

     service munin-node restart

=back

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::UserAgent;

my $PLUGIN_NAME = 'jitsi_total_failed_conferences';


##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "graph_title Jitsi failed conferences\n";
    print "graph_vlabel Total of failed conferences on JitsiMeet\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category jitsi\n";
    print "graph_info This graph shows the total number of failed conferences\n";
    print "total_failed_conferences.label Total number of failed conferences\n";
    print "total_failed_conferences.draw AREA\n";
    munin_exit_done();
}

##### fetch
my $ua = Mojo::UserAgent->new;
my $value = $ua->get('http://localhost:8080/colibri/stats')->result->json->{total_failed_conferences};
print "total_failed_conferences.value $value";
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
