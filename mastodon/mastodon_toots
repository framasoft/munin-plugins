#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  mastodon_toots - Used to track how many toots are emitted from your Mastodon instance

=head1 AUTHOR AND COPYRIGHT

  Copyright 2016 Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/mastodon_toots

     [mastodon_toots]
     user mastodon
     env.dbname mastodon_production # default to mastodon_production

=item - /etc/munin/plugins

     ln -s mastodon_toots /etc/munin/plugins/mastodon_toots

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojo::Pg

  Although it is certainly available in your GNU/Linux packages, it recommended to install it trough the cpan command:

     cpan Mojo::Pg

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::Pg;
use Storable;
use POSIX;

my $PLUGIN_NAME = 'mastodon_toots';

my $dbname = $ENV{'dbname'} || 'mastodon_production';
my $pg = Mojo::Pg->new('postgresql:///'.$dbname);

my $cachefile = $Munin::Plugin::pluginstatedir.'/cache_'.$PLUGIN_NAME;

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "graph_title Mastodon toots\n";
    print "graph_vlabel Number of toots on Mastodon\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category mastodon\n";
    print "graph_info This graph shows the number of toots emitted from your Mastodon instance\n";
    print "toots.label Toots\n";
    print "toots.draw AREA\n";
    munin_exit_done();
}

if( (defined $ARGV[0]) && ($ARGV[0] eq 'cron') ) {
    populate_cache();
    munin_exit_done();
}

if (-f $cachefile) {
    my $mtime = (stat($cachefile))[9];
    if (time - $mtime > 86400) {
        unlink $cachefile;
    }
}

unless (-f $cachefile) {
    populate_cache();
}

my $cache = retrieve($cachefile);
print "toots.value ".$cache->{'toots.value'}."\n";

munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub populate_cache {
    my $result = $pg->db->query('SELECT COUNT(s.id) FROM statuses s JOIN accounts a ON a.id = s.account_id WHERE a.domain IS NULL')->hashes->first;
    store { 'toots.value' => $result->{count} }, $cachefile;
} ## sub populate_cache

sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
