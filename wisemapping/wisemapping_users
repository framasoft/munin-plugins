#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  Wisemapping_users — get some stats about a wisemapping instance

=head1 AUTHOR AND COPYRIGHT

  Copyright 2022 Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/wisemapping

     [wisemapping_*]
     user root
     env.dbname wisemapping # default to wisemapping

=item - /etc/munin/plugins

     ln -s wisemapping_users /etc/munin/plugins/wisemapping_users

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojo::mysql:

     cpan Mojo::mysql

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::mysql;
use Storable;
use POSIX;

my $PLUGIN_NAME = 'wisemapping_users';

my $dbname = $ENV{'dbname'} || 'wisemapping';
my $mysql = Mojo::mysql->strict_mode(sprintf 'mysql:///%s', $dbname);

my $cachefile = $Munin::Plugin::pluginstatedir.'/cache_'.$PLUGIN_NAME;

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "graph_title Wisemapping users stats\n";
    print "graph_vlabel Users number\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category wisemapping\n";
    print "graph_total Total\n";
    print "graph_info This graph shows statistics about users on your wisemapping ance\n";
    print "activated.label Activated users\n";
    print "activated.draw AREA\n";
    print "unactivated.label Unactivated users\n";
    print "unactivated.draw STACK\n";
    munin_exit_done();
}

my $to_cache = {};

if (-f $cachefile) {
    my $cache = retrieve($cachefile);
    printf "activated.value %d\n", $cache->{'activated.value'};
    printf "unactivated.value %d\n", $cache->{'unactivated.value'};
    unless (-f $cachefile.'-lock') {
        my $pid = fork();
        if (not $pid) {
            POSIX::setsid();
            fork and exit;
            open my $lockfile, '>', $cachefile.'-lock';
            close $lockfile;

            my $result = $mysql->db->query('SELECT count(*) AS count FROM USER WHERE activation_date IS NOT NULL;')->hashes->first;
            $to_cache->{'activated.value'} = $result->{count};

            $result = $mysql->db->query('SELECT count(*) AS count FROM USER WHERE activation_date IS NULL;')->hashes->first;
            $to_cache->{'unactivated.value'} = $result->{count};

            store $to_cache, $cachefile;
            unlink $cachefile.'-lock';
        } else {
            waitpid $pid, 0;
        }
    }
} else {
    ##### fetch
    my $result = $mysql->db->query('SELECT count(*) AS count FROM USER WHERE activation_date IS NOT NULL;')->hashes->first;
    print "activated.value ".$result->{count}."\n";
    $to_cache->{'activated.value'} = $result->{count};

    $result = $mysql->db->query('SELECT count(*) AS count FROM USER WHERE activation_date IS NULL;')->hashes->first;
    print "unactivated.value ".$result->{count}."\n";
    $to_cache->{'unactivated.value'} = $result->{count};

    store $to_cache, $cachefile;
}

munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
